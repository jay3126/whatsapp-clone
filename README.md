# Meteor-Ionic-Angular
WhatsApp clone project using Meteor framework following the `Meteor CLI` path
from the [angular-meteor.com](https://angular-meteor.com/tutorials/whatsapp-tutorial)
website.

## Docker
To run the project using docker, simply run:

```bash
$ docker-compose up
```

If you want to add meteor/npm dependencies to the project, the commands should
be run through the `docker-exec` interface:

```bash
$ docker exec <container-id> <meteor-command> <arg1> <arg2> ...
```
