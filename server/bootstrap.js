import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';

Meteor.startup(function() {
    if (Meteor.users.find().count() > 3) return false;

    Accounts.createUserWithPhone({
        phone: '+972501234567',
        password: 'secret',
        profile: {
            name: 'My friend 1'
        }
    });

    Accounts.createUserWithPhone({
        phone: '+972501234568',
        password: 'secret',
        profile: {
            name: 'My friend 2'
        }
    });

    Accounts.createUserWithPhone({
        phone: '+972501234569',
        password: 'secret',
        profile: {
            name: 'My friend 3'
        }
    });
});
