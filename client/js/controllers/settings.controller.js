import { _ } from 'meteor/underscore';
import { Controller } from 'angular-ecmascript/module-helpers';

export default class SettingsCtrl extends Controller {

    logout() {
        Meteor.logout((err) => {
            if (err) return this.handleError(err);

            this.$state.go('login');
        });
    }

    handleError(err) {
        this.$log.error('Logout error: ', err);

        this.$ionicPopup.alert({
            title: err.reason || 'Logout failed',
            template: 'Please try again',
            okType: 'button-positive button-clear'
        });
    }

}

SettingsCtrl.$name = 'SettingsCtrl';
SettingsCtrl.$inject = [ '$state', '$ionicPopup', '$log' ];
